
import itertools

import time
import datetime
from random import randint
from random import shuffle
from random import sample
from time import sleep

zipCodeList = [ str(randint(10000,99999)) for num in range(50)]
city = itertools.cycle(sample(['Valencia', 'New York', 'Sacramento', 'Austin', 'Dallas', 'Toronto', 'Las Vegas'], 7))
state = itertools.cycle(sample(['California', 'Arizona', 'Colorado', 'Florida', 'Texas', 'Alaska', 'Nevada'], 7))
zipCode = itertools.cycle(zipCodeList)
firstname = itertools.cycle(sample(['John', 'Russel', 'Rupert', 'Micheal', 'Jason', 'Steve', 'Albert'], 7))
lastname = itertools.cycle(sample(['Smith', 'Bert', 'Whitehead', 'Spira', 'Blanc', 'Maximilium', 'Ross'], 7))

level = itertools.cycle(sample(['TRACE', 'DEBUG', 'ERROR', 'INFO', 'FATAL'], 5))

time = 10000000

with open('log/luis.log', 'a+') as f:
  # Note that f has now been truncated to 0 bytes, so you'll only
  # be able to read data that you wrote earlier...
  for second in range(time):
    logStr = "%s %s [%s] - %s, %s\n" %  \
      (datetime.datetime.now(), next(level), next(zipCode), next(city), next(state))
    sleep(.1)
    print(logStr, end='', flush=True)
    f.write(logStr)


def log(time):
  """" log random data for 'time' in seconds """
  for second in range(time):
    logStr = "%s %s [%s] - %s, %s \n" %  \
      (datetime.datetime.now(), next(level), next(zipCode), next(city), next(state))
    sleep(1)


