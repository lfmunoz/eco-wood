
################################################################################
# Global Variables
################################################################################
CURRENT_DIR = $(shell pwd)
URL=oc112-16.maas.auslab.2wire.com
PORT=9200
IP=10.37.120.197
#URL=m10.auslab.2wire.com
#USER=logstash_system

################################################################################
# Elasticsearch
################################################################################
# service elasticsearch start
#docker run -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" docker.elastic.co/elasticsearch/elasticsearch:6.2.0
#docker pull docker.elastic.co/elasticsearch/elasticsearch:6.2.0

# list indices
list:
	curl -XGET "${URL}:${PORT}/_cat/indices?v&pretty" | sort -n -r -k3

# delete indices
del:
	curl -XDELETE "${URL}:${PORT}/dmc*"

LICENSE_FILE=luis-munoz-5f2872cb-5657-4bbc-bef9-5da98bbf04de-v5.json

# show license status
license:
	curl -XGET "${URL}:${PORT}/_xpack/license" 

license-put:
	curl -XPUT -u elastic "http://${URL}:${PORT}/_xpack/license" -H "Content-Type: application/json" -d @${LICENSE_FILE}

license-ack:
	curl -XPUT -u elastic "http://${URL}:${PORT}/_xpack/license?acknowledge=true" -H "Content-Type: application/json" -d @${LICENSE_FILE}

################################################################################
# Kibana
################################################################################
# service kibana start
# docker pull docker.elastic.co/kibana/kibana-oss:6.2.0

################################################################################
# Curator
################################################################################
.PHONY: curator

curator:
	@echo 'Dry Run, use "make curator-run" for actual run'
	curator --config curator/curator.yml --dry-run curator/clean_action.yml

curator-run:
	curator --config curator/curator.yml curator/clean_action.yml

################################################################################
# Filebeat
################################################################################
LOG_DIR=live

.PHONY: filebeat filebeat_bash

filebeat_bash:
	docker run --rm --user 0 -v ${CURRENT_DIR}/filebeat/filebeat.yml:/usr/share/filebeat/filebeat.yml -v ${CURRENT_DIR}/live/:${CURRENT_DIR}/live/ -it --net=host docker.elastic.co/beats/filebeat:6.1.2 /bin/bash

filebeat:
	docker run --rm -v ${CURRENT_DIR}/filebeat/filebeat.yml:/usr/share/filebeat/filebeat.yml -v ${CURRENT_DIR}/${LOG_DIR}/:${CURRENT_DIR}/${LOG_DIR}/ --net=host docker.elastic.co/beats/filebeat:6.1.2

################################################################################
# Logstash
################################################################################
#LOGSTASH_CONF_PROD=${CURRENT_DIR}/logstash/logstash_dmc.conf
LOGSTASH_YML=${CURRENT_DIR}/logstash/logstash.yml

LOGSTASH_CONF_PROD=${CURRENT_DIR}/logstash/logstash_usp.conf

LOGSTASH_CONF_TEST=${CURRENT_DIR}/logstash/logstash_test.conf
LOGSTASH_TEST_STDIN=${CURRENT_DIR}/logstash/test/dmc_testStdin.log
LOGSTASH_TEST_STDOUT=${CURRENT_DIR}/logstash/test/dmc_testStdout.log

.PHONY: logstash logstash-bash logstash-test

logstash-test:
	cat ${LOGSTASH_TEST_STDIN} | docker run --rm -i --net=host \
	-v ${LOGSTASH_CONF_TEST}:/usr/share/logstash/pipeline/logstash.conf \
	-v ${LOGSTASH_YML}:/usr/share/logstash/config/logstash.yml \
	docker.elastic.co/logstash/logstash:6.1.2 | pv > ${LOGSTASH_TEST_STDOUT} & wait; \
	nosetests logstash/test/TestDmc.py --nocapture --nologcapture --debug=[TestKPI]

logstash:
	docker run --rm -it --net=host \
	-v ${LOGSTASH_CONF_PROD}:/usr/share/logstash/pipeline/logstash.conf \
	-v ${LOGSTASH_YML}:/usr/share/logstash/config/logstash.yml \
	docker.elastic.co/logstash/logstash:6.1.2

logstash-bash:
	docker run --rm -it --net=host \
	-v ${LOGSTASH_CONF_PROD}:/usr/share/logstash/pipeline/logstash.conf \
	-v ${LOGSTASH_YML}:/usr/share/logstash/config/logstash.yml \
	docker.elastic.co/logstash/logstash:6.1.2 /bin/bash

logstash-status:
	curl -XGET "${IP}:9600/?pretty"

logstash-status-event:
	curl -XGET "${IP}:9600/_node/stats/events?pretty"

logstash-status-pipe:
	curl -XGET "${IP}:9600/_node/stats/pipelines?pretty"

check:
	nosetests logstash/test/TestDmc.py --nocapture --nologcapture --debug=[TestKPI] 

################################################################################
# end of file
################################################################################