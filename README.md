# Eco Wood 

This project is for collecting log files and metrics from all ECO microservices. It uses the elastic stack: 

* Elasticsearch - document storage
* Curator - manages elasticsearch indices, in particular deletes documents that are older than a specified time period. Needed so that storage requirements don't continously grow. 
* filebeat - collects log files and sends them to logstash
* Logstash - accepts log files from multiple filebeat applications running on different machines and parses them seperating out the log file fields (date, log level, exception, etc)
* Kibana - data visualization and exploration



# Installation and Configuration 

```bash
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
echo "deb https://artifacts.elastic.co/packages/6.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-6.x.list
sudo apt-get update
```

```bash
apt-get install elasticsearch
apt-get install kibana
```


### Elastic Search

http://oc112-16.maas.auslab.2wire.com:9200/

### Kibana

Install using apt-get or download .rpm or .deb from the elasticsearch website. 
Once Kibana is running in "Saved Objects" import the three JSON files under the kibana folder. 

http://oc112-16.maas.auslab.2wire.com:5601/

### Logstash

```bash
docker run --rm -it --net=host \
	-v ${LOGSTASH_CONF_PROD}:/usr/share/logstash/pipeline/logstash.conf \
	-v ${LOGSTASH_YML}:/usr/share/logstash/config/logstash.yml \
	docker.elastic.co/logstash/logstash:6.1.2
```
or

```bash
deb https://artifacts.elastic.co/packages/6.x/apt stable main
sudo apt-get update && sudo apt-get install logstash
sudo service logstash start
```

*  logstash.yml - contains configuration for the logstash service
*  logstash_dmc.conf - contains the pipeline for the dmc log files
*  logstash_sms.conf - contains the pipeline for the sms log files

#### DMC Log file format

[TIMESTAMP_ISO8601] [LOGLEVEL] [TREAD] [USERNAME:DEVICEID:SESSIONID:WORKFLOWNAME] LOGGER LOGMSG

LOGMSG can optionally be 

[LOGMSG]\n[JAVACLASS][STACKTRACE]

### Filebeat

The dmc logs are generally stored in
```
/bvt/cms/logs/dmc_cms*.log
```
The filebeat_prod.yml configuration file points to those files. We  simply run filebeat using docker and the configuration file.
```bash
docker run -d --name dmc_filebeat -v ${HOME}/filebeat_prod.yml:/usr/share/filebeat/filebeat.yml -v ${LOG_DIR}/:${LOG_DIR}/ --net=host docker.elastic.co/beats/filebeat:6.1.2 
```

### Curator

```bash
deb [arch=amd64] https://packages.elastic.co/curator/5/debian stable main
apt-get update 
apt-cache policy elasticsearch-curator
sudo apt-get install elasticsearch-curator
curator --version
curator, version 5.4.1
curator [--config CONFIG.YML] [--dry-run] ACTION_FILE.YML
```

Curator has two files that need to be edited; a configuration file and an action file

*  Configuration file - contains client connection and logging settings
*  Action file - tasks to run against an elasticsearch index

