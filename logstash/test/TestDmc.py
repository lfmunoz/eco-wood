################################################################################
################################################################################
# Description:
#  Logstash ECO Manage pattern test

# https://docs.python.org/3/library/unittest.html
################################################################################
################################################################################

######################################################################
# Imports
######################################################################
# NoseTest
from nose.tools import nottest
from nose.tools import assert_equal
from nose.tools import assert_not_equal
from nose.tools import assert_raises
from nose.tools import assert_in
from nose.tools import raises
from nose.tools import assert_not_in
import sys
import json
import logging

######################################################################
# TestDmc
######################################################################
class TestDmc(object):

  ################################################################################
  # Initialization Methods
  ################################################################################
  @classmethod
  def setup_class(self):
    """This method is run once for each class before any tests are run"""
    self.log = logging.getLogger("[TestDmc]")
    self.log.debug("--------------------------------------------------------------------------------")
    self.log.debug("--------------------------------------------------------------------------------")
    self.log.debug(" Running TestDmc ")
    self.log.debug("--------------------------------------------------------------------------------")
    self.log.debug("--------------------------------------------------------------------------------")
    self.log.debug("")
    filename = "logstash/test/dmc_testStdout.log"
    with open(filename, 'r+') as f:
      clean = ''.join(f.readlines()[13:-1])
      json_string = '[' + clean.replace('}\n', '},\n')[:-2] + ']'
      #print(json_string)
      self.data = json.loads(json_string)

  ################################################################################
  # Tests 
  ################################################################################
  def test_kpi_schema(self):
    # make sure data is sorted by date
    self.data.sort(key=lambda x: x["@timestamp"])
    # print(json.dumps(self.data, indent=4, sort_keys=True))
    assert_equal(6, len(self.data))
    assert_equal("Failed to send list of device informs.", self.data[0]["logMsg"])
    assert_equal("Error in workflow Pace Sync Components. Message is No response from device. Session timed out", self.data[1]["logMsg"])
    assert_equal("In VoIP Workflow: Subscriber Services: ", self.data[2]["logMsg"])
    assert_equal("Transport (tcp://10.89.24.25:61626) failed, reason:  java.io.EOFException, attempting to automatically reconnect", self.data[3]["logMsg"])
    assert_equal("Error in workflow Delete PortMapping. Message is Traceback (most recent call last):", self.data[4]["logMsg"])
    assert_equal("Error in workflow Frontier GPV to ECO Inquire. Message is java.net.ConnectException: Connection refused", self.data[5]["logMsg"])
    #assert_in('workflow', self.data[1]["tags"])
    # assert_not_in('tags', self.data[0])



################################################################################
# End of File
################################################################################



# [CWMP-processor-34] 
# [pool-5-thread-3] 
# [int-http-878] 
# [CWMP-processor-6] 
# [int-http-853] 
# [ext-http-294] 
# [int-http-791] 
# [ContainerBackgroundProcessor[StandardEngine[External]]] 
# [ActiveMQ Transport: tcp:///10.89.24.25:61626@37974]
# [New I/O  worker #15]

# [00D09E-46141N009417:23269987:1:Provision VoIP]
# [00D09E-431219021601:11492335:11:Configure Periodic Statistics] 
# [00D09E-261239010685:9944172:11:2Wire Remote Bridge Firmware Update-findInidNames]
# [00D09E-42161N045905:94659297:C3B915657D9E7B8971DC0B16B40A98E1:4:Bulk Data-gatherBulk]
# [001E46-154478084784784:20207789:780E37334D71AB0AC3E7DA7929A4125F:1:Transfer Complete]
# [webservice_att_g2_Node12-g2.pltnca.sbcglobal.net::::]
# [::::] 
# [webservice_att_isaac:::] 
